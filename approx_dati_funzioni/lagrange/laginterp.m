function [ c ] = laginterp(x,y)
% ----------------------------------------------
% Funzione che calcola i coefficienti 
% del polinomio di Lagrange
%-----------------------------------------------
% Input: vettori [x],[y]
% Output:[c] vettore coefficienti
% il polinomio f(x) di Lagrange e' ottenuto
% f(x)= sum(c_i * prod((x-x_j)))
%-----------------------------------------------
ns=size(x);
n=ns(2)-1;
for i=1:(n+1)
    pr=1;
    for j=1:(n+1)
        if(i~=j)
            pr=pr*(x(i)-x(j));
        end
    end
    c(i)=(y(i))/(pr);
end
end

