function [l,f] = polilagrange(c ,x,t)
% ----------------------------------------------
% Funzione che valuta il polinomio di Lagrange f
% ed i polinomi accessori nell'ascissa t
%-----------------------------------------------
% Input: vettori [x],[c] | t
% Output:vettore [l] (valore dei polinomi accessori) f=sum(l)
%-----------------------------------------------
ns=size(x);
n=ns(2)
for i=1:n
p=1;
for j=1:n
    if(i~=j)
    p=p*(t-x(j));
    end
end
l(i)=c(i)*p;
end
f=sum(l);
end

