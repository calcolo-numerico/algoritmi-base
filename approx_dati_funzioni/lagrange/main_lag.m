%  @author  Pietro Pennestri'
%  @websit http://www.pennestri.me
%  @email pietro.pennestri@gmail.com
%  @version  1.0

clc
%y=sin(3*x)
x=[1 1.3 1.6 1.9 2.2];
y=[0.1411 -0.6878 -0.9962 -0.5507 0.3115];
tic
[c] = laginterp(x,y)
npoints=100
t= linspace(0,pi,npoints);
[l,f] = polilagrange(c ,x,1.5) 
for i=1:npoints  
   [l,f] = polilagrange(c ,x,t(i)) 
   fun(i)=f;
   funext(i)=sin(3*t(i));
end
plot(t,fun)
hold on
plot(t,funext)
hold on
scatter(x,y)
hold off

legend('Pol. Lagrange','sin(3x)','Dati')
toc
