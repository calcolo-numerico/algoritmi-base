%  @author  Pietro Pennestri'
%  @website http://www.pennestri.me
%  @email pietro.pennestri@gmail.com
%  @version  1.0

% main_potenze.m main che richiama la funzione potenze.m
A=[1 -1 2;-2 0 5;6 -3 6]
u0=[1;1;1]
tol=1.e-15 % tolleranza
N=200 % numero massimo di iterazione
tic
[lambda1,it2,iter] = potenze(A,u0,tol,N)
toc

