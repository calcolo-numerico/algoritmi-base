function [lambda1,it2,iter] = potenze(A,u0,tol,nmax)
% Metodo della potenza
% Parametri input
% A: Matrice quadrata
% u0: Stima iniziale autovettore
% tol: Parametro tolleranza
% nmax: Numero massimo di iterazioni
% Parametri output
% lambda1 : Stima autovalore
% it2: Stima autovettore
% iter: Numero iterazioni
it2=u0;
lambda1=0;  % assegno valore arbitrario, che non entra nella successione 
res=1+tol;
iter=0;
while (res>tol & iter<nmax)
    lambda0=lambda1;
    it1=it2;
    it1=(it1)/norm(it1);
    it2=A*it1;
    it2=(it2);
    lambda1=(it2(1))/(it1(1));
    if (iter == 0)
        iter=iter+1;
    else
        res=abs(lambda1-lambda0);
        iter=iter+1;
    end
    
end

if (res < tol)
    return;
else
      disp('-Errore--------')
end


end

