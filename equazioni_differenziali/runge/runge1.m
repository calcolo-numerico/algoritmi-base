function [y,t]=runge1(fun,n,t0,t1,y0)
% fun : Funzione da integrare
% n : numero di passi di integrazione
% t_0, t_1 : Valore iniziale e finale del parametro indipendente
% y_0 : Valore iniziale
h=(t1-t0)/n; % Passo integrazione
t(1)=t0;
y(1)=y0;
for i=1:n
t(i+1)=t(i)+h;
y_h=y(i)+fun(t(i),y(i))*h/2.;
t_h=t(i)+h/2.;
s_h=fun(t_h,y_h); % Coeff. angolare punto mezzeria
y(i+1)=y(i)+h*s_h;
end;
end  %End of function
