%  @author  Pietro Pennestri'
%  @websit http://www.pennestri.me
%  @email pietro.pennestri@gmail.com
%  @version  1.0
%
% Integrazione equazioni differenziali
% con metodo di Eulero
clear all
close all
t0=0  % Tempo iniziale
t1=2  % Tempo finale
y0=1  % Valore iniziale
n=100 % Numero intervalli
tic
[y,t]=euler1(@fun,n,t0,t1,y0);
toc
h=(t1-t0)/n;
for i=1:n+1
t(i)=t0+h*(i-1);
yprime(i)=fun(t(i),y(i));    
end
V=[t',y']
yyaxis left
plot(t,y,'LineWidth',2);
ylabel('y')
yyaxis right
plot(t,yprime,'--','LineWidth',2)
ylabel('dy/dt')
grid on
title('Metodo Eulero - Curve integrale (y) e derivata (dy/dt)')
legend('y ','dy/dt','Location','northeast')
xlabel('t') % x-axis label
