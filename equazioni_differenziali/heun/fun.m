function yprime=fun(t,y)
yprime=(t^2-y^2)*sin(y);
end % End of function