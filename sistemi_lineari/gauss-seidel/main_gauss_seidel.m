%  @author  Pietro Pennestri'
%  @websit http://www.pennestri.me
%  @email pietro.pennestri@gmail.com
%  @version  1.0

clc
 
% A=[1 2 -4; 2 -1 1; 1 1 3]; % matrice dei coefficienti
A=[4 -1 -1 ;-2 6 1;-1 1 7]
D=diag(diag(A))
U=triu(A)-D
L=tril(A)-D
C=-inv(D)*(L+U)
out=eig(C)
n=length(out);
Nmax=1000;
tol=1e-4;


tt=[];
for i=1:n
    normaelemento=abs(out(i));
tt=[tt;
    normaelemento];
end
test=max(tt)
if (test<1)
    b=[3 ; 9; -6]; % vettore termini noti
    x0=[0;0;0]; % vettore di prima iterazioneNmax=100;
    tol=1e-4;
    tic
    [x,iter] = gauss_seidel(A,b,x0,Nmax,tol);
    toc

    disp('----------RISULTATI----------------')
    disp('Matrice dei coefficienti A')
    fprintf([repmat('%f\t', 1, size(A, 2)) '\n'], A');
    disp('Vettore dei termini noti b')
    fprintf([repmat('%f\t', 1, size(b, 2)) '\n'], b');

    disp('Vettore soluzione x')
    fprintf([repmat('%f\t', 1, size(x, 2)) '\n'], x');

    fprintf('Tempo esecuzione %10e \n', toc) ;
    test=A*x-b;
    disp('Verifica il risultato test=A*x-b')
    disp('Matrice test')
    fprintf([repmat('%f\t', 1, size(test, 2)) '\n'], test');
    disp('-----------------------------------')
else
    error('Raggio spettrale maggiore di 1')
end