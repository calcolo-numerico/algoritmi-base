function [ L,U] = LUdolittle( A )
% ----------------------------------------------
% Funzione che implementa il metodo di Dolittle
% per la fattorizzazione LU senza pivot
%-----------------------------------------------
% Input: Matrice quadrata A non singolare
% Output:
% L,U: Matrici tali che A=LU
%-----------------------------------------------
n=length(A);
U=zeros(n,n);
L=eye(n);
for k=1:n
    for m=k:n
         U(k,m)=A(k,m);
        if k>1
        U(k,m)=A(k,m)-dot(L(k,1:(k-1)),U(1:(k-1),m));
        end
    end
    for i=k+1:n
       L(i,k)=(A(i,k))/U(k,k);
        if k>1
       L(i,k)=(A(i,k)-dot(L(i,1:(k-1)),U(1:(k-1),k)))/U(k,k);
        end
    end          
    end
end