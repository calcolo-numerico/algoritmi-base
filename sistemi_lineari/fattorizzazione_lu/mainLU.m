%  @author  Pietro Pennestri'
%  @websit http://www.pennestri.me
%  @email pietro.pennestri@gmail.com
%  @version  1.0

% main_LU.m main che richiama la funzione LUdolittle.m

% matrice da fattorizzare
clc
A=[ 3 2 -1 2;
     -3 -4 2 1;
     6 2 -2 11;
     -6 -10 6 2]
 tic
 [ L,U] = LUdolittle( A );
 toc
 T=L*U;
disp('----------RISULTATI----------------')
disp('Matrice L')
fprintf([repmat('%f\t', 1, size(L, 2)) '\n'], L');
disp('Matrice U')
fprintf([repmat('%f\t', 1, size(U, 2)) '\n'], U');
fprintf('Tempo esecuzione %10e \n', toc) ;
disp('Verifica il risultato [T]=[L]*[U]')
disp('Matrice T')
fprintf([repmat('%f\t', 1, size(T, 2)) '\n'], T');
disp('-----------------------------------')