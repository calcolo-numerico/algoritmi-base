function [h]=cholesky(a)
%-----------------------------------
% INPUT
% a: matrice simmetrica definita positiva
%
% OUTPUT
% l: matrice triangolare inferiore tale  che l*l'=a
%
% 
%-----------------------------------
test=isPositiveDefinite(a); % Controlla che la matrice sia simmetrica 
%                            positiva definita
n=size(a);
h(1,1)=sqrt(a(1,1));
for i=2:n,
for j=1:i-1,
s=0;
for k=1:j-1,
s=s+h(i,k)*h(j,k);
end
h(i,j)=1/h(j,j)*(a(i,j)-s);
end
h(i,i)=sqrt(a(i,i)-sum(h(i,1:i-1).^2));
end
return