%  @author  Pietro Pennestri'
%  @websit http://www.pennestri.me
%  @email pietro.pennestri@gmail.com
%  @version  1.0

% mainCholesky.m main che richiama la funzione cholesky.m
clc

% A deve essere una matrica simmetrica e 
% definita positiva!
A=[ 2 -1 0;
     -1 2 -1;
     0 -1 2]
 tic
  L = cholesky( A );
 toc
 T=L*L';
 
 
disp('----------RISULTATI----------------')
disp('Matrice L')
fprintf([repmat('%f\t', 1, size(L, 2)) '\n'], L');
fprintf('Tempo esecuzione %10e \n', toc) ;
disp('Verifica il risultato [T]=[L]*[L]^t')
disp('Matrice T')
fprintf([repmat('%f\t', 1, size(T, 2)) '\n'], T');
disp('-----------------------------------')