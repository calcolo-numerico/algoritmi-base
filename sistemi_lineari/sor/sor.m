function [x,iter] = gauss_seidel(A, b, x0, itermax, tol ,omega)
% ----------------------------------------------
% Funzione che implementa il metodo iterativo
% Rilassamento - SOR
%-----------------------------------------------
% Inputs
% A, b: matrice e termine noto, rispettivamente
% x0 : soluzione iniziale
% tol : tolleranza calcoli
% itermax: massimo numero iterazioni
% omega: parametro per migliorare co
%
% Outputs
% x : vettore soluzione
% iter: numero delle iterazioni
%-----------------------------------------------
    n = length(b);
    x=x0;
    iter=0;
    res=1+tol;
    while (res>tol & iter<itermax)
        for i=1:n
            sum=0;
            for j=1:n
                if(j~=i)
                    sum=sum+A(i,j)*x(j);
                end
            end
            v(i)=(-sum+b(i))/A(i,i);
            x(i)=omega*v(i)+(1-omega)*x0(i);
        end
        x0=x;
        res=norm(A*x-b);
        iter=iter+1;
    end
    if (res<tol)
        disp('il metodo giunge a convergenza')
        return;
    else
        error('il metodo non giunge a convergenza')
    end    