function f=fun(x)
% 
% INPUT
% x: Vettore delle incognite
% 
% OUTPUT
% f: Vettore dei residui
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
f(1)= x(1)^2+x(2)^2-1;
f(2)= x(1)-x(2);
f=f'; % Vettore f in colonna
end
