function [x,iterations,f,resid,exitflag]=newtonraphson(fun,jacobian,x0,tol,itmax)
%
% INPUT
% fun : Function in cui vengono valutati gli elementi del vettore dei residui
% jacobian : Function in cui vengono valutati gli elementi della matrice jacobiana
% x0: Vettore soluzione di primo tentativo
% tol: Criterio convergenza
% itmax: Numero massimo di iterazioni
%
% OUTPUT
% x: Soluzione
% iterations: Numero di iterazioni eseguite
% f: Vettore dei reidui
% resid: Norma del vettore dei residui
% exitflag:  1 L'iterazione converge sul valore x, 0
%            0 Si e' superato il massimo numero di iterazioni
% Inizializza le variabili
resid=1000;
iterations=0;
while(resid>tol)
    % Calcola matrice Jacobiana
    J=feval(jacobian,x0);
    % Calcola residui
    f=feval(fun,x0);
    % Calcola norma vettore dei residui
    resid=norm(f);  
    % Trova il nuovo valore di x con il metodo di Newton-Raphson
    x = x0-inv(J)*f; % inv(J)  J \  
    % Controllo sulla tolleranza
    if(resid<tol)
        exitflag=1;
        return
    end
    % Controllo sul numero delle iterazioni
    if(iterations>itmax)
        exitflag=0;
        return
    end
    % Assegan il nuovo valore come condizioni iniziali per la successiva
    % iterazione
    x0=x;
    iterations=iterations+1;
end
end
