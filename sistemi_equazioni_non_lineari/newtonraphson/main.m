%
% Soluzione di sistemi di equazioni non lineari mediante iterazione di Newton-Raphson
%
clc; clear;
% Soluzione di primo tentativo
x0=[0.5;-0.3]; 
str = sprintf('Soluzione di primo tentativo: %.1f  %.1f',x0);
disp(str);
% Criterio convergenza
tol=1e-6;
str = sprintf('Tolleranza: %.2e',tol);
disp(str);
% Massimo numero di iterazioni
itmax=100;
str = sprintf('Massimo numero di iterazioni: %i',itmax);
disp(str);
str= sprintf ( '\n\n----Metodo Newton-Raphson----\n\n');
disp(str);
% Metodo di Newton-Raphson per trovare la soluzione del sistema di
% equazioni non lineari
tic
[x,iterazioni,funzioni,residui,exitflag]=newtonraphson(@fun,@jacobian,x0,tol,itmax);
toc
str = sprintf('Vettore Soluzione = %.2e %.2e',x);
disp(str);
str = sprintf('Iterazioni = %.i',iterazioni);
disp(str);
str = sprintf('Residui della funzione = %.3e %.3e',funzioni);
disp(str);
str = sprintf('Massima norma dei residui = %.3e',residui);
disp(str);
fprintf('Tempo esecuzione %10e \n', toc) ;
