function J = jacobian(x)
%
% INPUT
% x: Vettore delle incognite
% 
% OUTPUT
% J: Matrice Jacobiana
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
J(1,1)= 2*x(1);
J(1,2)= 2*x(2);
J(2,1)= 1;
J(2,2)= -1;
end
