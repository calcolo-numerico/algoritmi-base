%  @author  Pietro Pennestri'
%  @websit http://www.pennestri.me
%  @email pietro.pennestri@gmail.com
%  @version  1.0
%
% Integrazione con formula di Simpson
clear all
n = 5
% Numero punti (dispari)
a = -2                                % Coordinata estremo sinistro intervallo integrazione
b = 2                                % Coordinata estremo destro intervallo integrazione
x = linspace(a, b, n)                  % Genera valori delle ascisse punti intervallo
y=computefun(x)                        % Calcola i valori della funzione nei punti dell'intervallo                                       
tic                                       
A=y(2:n-1);   % Vettore y con estremi esclusi                                     
yeven = A(1:2:end); % Estrai elementi con indici dispari
yodd  = A(2:2:end); % Estrai elementi con indici pari                                     
sum_y = (y(1)+2*sum(yodd)+4*sum(yeven)+y(n))   ;  % Applica la formula dei trapezi
dx = (b - a) / (n - 1) ;                % Calcola ampiezza intervalli elementari (base trapezio)
area = sum_y * dx/3.;                      % Calcola area 
toc
fprintf('Metodo di Simpson \n') ;
fprintf('Estremi integrazione %10e %10e: \n', a,b) ;
fprintf('Numero intervalli %i \n', n) ;
fprintf('Stima integrale %10e: \n', area) ;
fprintf('Tempo esecuzione %10e \n', toc) ;
