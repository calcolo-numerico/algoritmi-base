function [R,quad,err,h] = romber(f,a,b,n)
%---------------------------------------------------------------------------
%ROMBER   Quadratura con il metodo di Romberg 
% Inputs
%      f:      procedura per valutare funzione da integrare
%    a,b:    estremi intervallo integrazione
%     n :     massimo numero di righe nella tabella
% Return
%   R:      tabella di Romberg
%   quad    stima integrale 
%   err     errore nella stima integrale
%   h       ampiezza intervallo impiegato
%
%---------------------------------------------------------------------------
% Prima colonna
%--------------------
R=zeros(n,n);
for j=1:n,
m=2^(j-1); 
h=(b-a)/m;
x=linspace(a,b,m+1);
ff=f(x);
R(j,1)=(h/2)*(ff(1)+ff(end)+2*sum(ff(2:end-1))); % Applica Trapezi
end;
%--------------------
% Colonne successive
%--------------------
for k=2:n,
for i=k:n,
R(i,k)=(4^(k-1)*R(i,k-1)-R(i-1,k-1))/(4^(k-1)-1);
end;
end;
% 
% % Stima Errore
 err = abs(R((n),(n-1))-R(n,n));
% end
quad = R(n,n);
end % End