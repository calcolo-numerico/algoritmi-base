%  @author  Pietro Pennestri'
%  @websit http://www.pennestri.me
%  @email pietro.pennestri@gmail.com
%  @version  1.0
%
% Integrazione con formula di Romber
clear all
n = 4
% Estremi di integrazione
a = -2                                
b = 2 
tic
[R,area,err,h] = romber(@computefun,a,b,n);
toc
fprintf('Metodo di Romber \n') ;
fprintf('Estremi integrazione %10e %10e: \n', a,b) ;
fprintf('Numero righe tabella %i \n', n) ;
fprintf('Tabella di Romberg \n') ;
R
fprintf('Stima integrale %10e: \n',area ) ;
fprintf('Tempo esecuzione %10e \n', toc) ;