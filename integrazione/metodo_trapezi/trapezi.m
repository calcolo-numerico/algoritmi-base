%  @author  Pietro Pennestri'
%  @websit http://www.pennestri.me
%  @email pietro.pennestri@gmail.com
%  @version  1.0
%
% Integrazione con formula dei trapezi
n = 5                                % Numero punti
a = -2                                % Coordinata estremo sinistro intervallo integrazione
b = 2                                % Coordinata estremo destro intervallo integrazione
tic
x = linspace(a, b, n) ;                 % Genera valori delle ascisse punti intervallo
y=computefun(x)                           % Calcola i valori della funzione nei punti dell'intervallo
                                        % 
sum_y = sum(y) - (y(1) + y(end)) ./ 2;  % Applica la formula dei trapezi
dx = (b - a) / (n - 1) ;                % Calcola ampiezza intervalli elementari (base trapezio)
area = sum_y * dx;                      % Calcola area 
toc
fprintf('Metodo dei trapezi \n') ;
fprintf('Estremi integrazione %10e %10e: \n', a,b) ;
fprintf('Numero intervalli %i \n', n) ;
fprintf('Stima integrale %10e: \n', area) ;
fprintf('Tempo esecuzione %10e \n', toc) ;
