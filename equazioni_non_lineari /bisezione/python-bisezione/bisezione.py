from __future__ import division
import numpy as np 
import math
def f(x):
	return x**2-math.exp(x)-1
	#return x**3

def errore(a,b,i):
	return abs(b-a)/2**i
a=-1.5
b=-1


iterazioni=100
errore_desiderato=0.001

i=0
ck=0
if f(a)*f(b)>0:
	print "ERRORE: Scegliere intervallo opportuno!"
	ck=1
else:
	while i<=iterazioni:
		c=(b+a)/2
		if(f(c)==0):
			print "La radice cercata:"+str(c)
			ck=1
			break
		elif errore(a,b,i)<=errore_desiderato:
			print "Approx con:"+str(c)+" la radice cercata"
			print "si ha errore di troncamento minore o uguale a"+ str(errore(a,b,i))
			ck=1
			break
		elif f(a)*f(c)<0:
			c=b
		elif f(b)*f(c)<0:
			a=c
		i=i+1

if ck==0:
	print "Numero di iterazioni max raggiunto!"
	print "Approx con:"+str(c)+" la radice cercata"
	print "si ha errore di troncamento minore o uguale a"+ str(errore(a,b,i))
