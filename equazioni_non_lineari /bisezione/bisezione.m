function [ r,iter ] = bisezione( f, a, b, N, tol )
% ----------------------------------------------
% Funzione che implementa il metodo di bisezione
%-----------------------------------------------
% InpUTS
% f: funzione con l'equazione da risolvere 
% a,b: estremi dell'intervallo di ricerca
% CRITERI DI ARRESTO
% N: numero massimo di iterazioni 
% tol: tolleranza
% Outputs
% r: soluzione (approx)
% k: numero di iterazioni
%-----------------------------------------------
    % Controlla se la radice coincide con uno degli estremi
    if ( f(a) == 0 ) 
	r = a;
    iter=0;
	return;
    elseif ( f(b) == 0 )
	r = b;
    iter=0;
	return;
    % controlla che l'intervallo sia opportuno
    elseif ( f(a) * f(b) > 0 )
        error( 'ERRORE: Scegliere un intervallo opportuno' );
    end
    
    % Inizia la ricerca della radice
    for k = 1:N
        % Trova il punto medio dell'intervallo 
        c = (a + b)/2;

        % Decidi quale intervallo scegliere
        %          [a, c] se f(a) e f(c) hanno segni opposti, o
        %          [c, b] se f(c) e f(b) hanno segni opposti.

        if ( f(c) == 0 )
            r = c;
            iter=k;
            return;
        elseif ( f(c)*f(a) < 0 )
            b = c;
        else
            a = c;
        end

        %controlla convergenza 
        if ( b - a < tol )
            if ( abs( f(a) ) < abs( f(b) ) && abs( f(a) ) < tol )
                r = a;
                iter=k;
                return;
            elseif ( abs( f(b) ) < tol )
                r = b;
                iter=k;
                return;
            end
        end
    end

    error( 'Il metodo non converge!' );
end